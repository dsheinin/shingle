# Shingle

Shingle is a custom-built portfolio website for software professionals. 
You can see it in action at [sheinin.ca](https://sheinin.ca/)

## Project Structure

Shingle uses a decoupled architecture. React/Next.js with Material-UI is used in the front end, 
and Django is working as a headless CMS. The glue is a REST API powered by Django REST 
framework.

## Why "Shingle"?

In the nineteenth century a "shingle" was a small wooden signboard announcing a 
professional's business. Times have changed.

## Development Installation

After cloning this repository...

### Django

1. Install a PostgreSQL server and create a database. (PostgreSQL is recommended but not required.
Any Django-supported database will do.)
1. Install [Poetry](https://python-poetry.org/docs/#installation).
1. Create and activate a python 3.6+ virtualenv outside the project root.
1. Copy `django/shingle/settings/local.py.SAMPLE` to `django/shingle/settings/local.py`. 
Adjust the DB connection settings, adding host and port settings if required. 
(See [Django DB settings documentation](https://docs.djangoproject.com/en/2.2/ref/settings/#databases).)
Create an API token as `NEXT_API_TOKEN`.
Make any other changes as needed. Create directories outside the project root corresponding to your `MEDIA_ROOT`,
`STATIC_ROOT` and `MEDIA_ROOT` settings.
1. cd to the `django` directory.
1. Run `./manage.py migrate`.
1. Create a superuser with `./manage.py createsuperuser`.
1. Start the development server with `./manage.py runserver` and access the cms at [localhost:8000/admin]().
Create the profile and optionally add other content.

### Next.js

1. Install [Node.js](https://nodejs.org/en/download/).
1. cd to the `nextjs` directory.
1. Run `npm install`.
1. Copy `.env.local.SAMPLE` to `.env.local`. Set `API_TOKEN` to the value of `NEXT_API_TOKEN`
entered previously in Django settings.
1. Start the development site with `npm run dev` and access it at [localhost:3000]().