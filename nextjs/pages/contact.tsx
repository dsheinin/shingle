import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader";
import { getProfile } from "../lib/api";
import { styled } from "@mui/material/styles";
import { Container, Typography, Grid, TextField, FormHelperText, Button, Alert } from '@mui/material';
import EmojiPeopleIcon from '@mui/icons-material/EmojiPeople';
import PhoneIcon from '@mui/icons-material/Phone';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import { Formik } from 'formik';
import * as Yup from "yup";
import axios from "axios";
import * as apiTypes from "../types/api";
import {GetStaticProps} from "next";
import HeadMetatags from "../components/HeadMetatags";

const Coordinates = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    fontSize: '2rem',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(6),
    '.name': {
        fontWeight: 'bold',
    },
    '.coord': {
        display: 'flex',
        lineHeight: 1.1,
        marginTop: theme.spacing(1),
        '.MuiSvgIcon-root': {
            verticalAlign: 'middle',
            marginRight: theme.spacing(2),
        },
    },
    [theme.breakpoints.down('sm')]: {
        justifyContent: 'flex-start',
        fontSize: '1.25rem',
    },
}));

// TODO: similar to about sections...consolidate.
const FormContainer = styled('div')(({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
    padding: `${theme.spacing(4)} ${theme.spacing(8)}`,
    marginBottom: theme.spacing(4),
    '& h2': {
        textTransform: 'uppercase',
        marginBottom: theme.spacing(4),
        textAlign: 'center',
    },
    [theme.breakpoints.down('md')]: {
        padding: `${theme.spacing(2)} ${theme.spacing(4)}`,
    },
    [theme.breakpoints.down('sm')]: {
        padding: `${theme.spacing(2)} ${theme.spacing(2)}`,
    },
}));

type ContactProps = {
    profile: apiTypes.Profile;
};

export default function Contact({ profile }: ContactProps) {
    const RESULT = {
        SUCCESS: 'success',
        FAILED: 'failed',
    }

    return (
        <>
            <HeadMetatags
                title="Contact"
                siteName={profile.name}
                description={profile.contactIntro}
            />
            <Layout profile={profile} activeSection="contact" dark>
                <Container>
                    <PageHeader heading="Contact" />
                </Container>
                <Container>
                    <Coordinates>
                        <div>
                            <div className="coord name"><EmojiPeopleIcon fontSize="inherit"/> {profile.name}</div>
                            {profile.phoneDisplay &&
                                <div className="coord"><PhoneIcon fontSize="inherit"/> {profile.phoneDisplay}</div>
                            }
                            {profile.email &&
                                <div className="coord"><AlternateEmailIcon fontSize="inherit"/> {profile.email}</div>
                            }
                            {profile.location &&
                                <div className="coord"><LocationOnOutlinedIcon fontSize="inherit"/> {profile.location}
                                </div>
                            }
                        </div>
                    </Coordinates>
                    <FormContainer>
                        <Typography variant="h2">Send a Message</Typography>
                        <Formik
                            validateOnMount
                            initialValues={{
                                name: '',
                                email: '',
                                message: '',
                            }}
                            validationSchema={Yup.object({
                                name: Yup.string()
                                    .max(100, "Must be 100 characters or less")
                                    .required("This field is required."),
                                email: Yup.string()
                                    .max(254, "Must be 254 characters or less")
                                    .email("This is not a valid email address.")
                                    .required("This field is required."),
                                message: Yup.string()
                                    .required("This field is required."),
                            })}
                            onSubmit={(values, actions) => {
                                actions.setStatus(RESULT.SUCCESS)
                                axios({
                                    method: 'post',
                                    url: `${process.env.NEXT_PUBLIC_API_BASE_URL}/contact/message/create/`,
                                    data: values,
                                })
                                    .then(response => {
                                        // console.log("message sent");
                                        actions.setStatus(RESULT.SUCCESS)
                                        actions.setSubmitting(false);
                                    })
                                    .catch(error => {
                                        // console.error("Message submission failed:", error);
                                        actions.setStatus(RESULT.FAILED)
                                        actions.setSubmitting(false);
                                    });
                            }} >
                            {formik =>
                                <form noValidate onSubmit={formik.handleSubmit} id="contact-form">
                                    <Grid container spacing={4}>
                                        <Grid item xs={12} md={6}>
                                            <TextField
                                                fullWidth
                                                id="name-field"
                                                label="Name"
                                                {...formik.getFieldProps('name')}
                                                error={formik.touched.name && 'name' in formik.errors}
                                                helperText={formik.touched.name ? formik.errors.name : null}
                                                required
                                                inputProps={{ maxLength: 100 }}
                                                disabled={formik.status === RESULT.SUCCESS}
                                            />
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <TextField
                                                fullWidth
                                                id="email-field"
                                                label="Email"
                                                {...formik.getFieldProps('email')}
                                                error={formik.touched.email && 'email' in formik.errors}
                                                helperText={formik.touched.email ? formik.errors.email : null}
                                                required
                                                inputProps={{ maxLength: 254 }}
                                                disabled={formik.status === RESULT.SUCCESS}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                id="message-field"
                                                label="Message"
                                                multiline
                                                minRows={5}
                                                {...formik.getFieldProps('message')}
                                                error={formik.touched.message && 'message' in formik.errors}
                                                helperText={formik.touched.message ? formik.errors.message : null}
                                                required
                                                disabled={formik.status === RESULT.SUCCESS}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                                            {formik.status === RESULT.SUCCESS ?
                                                <Alert severity="success">Message Received</Alert> :
                                                <>
                                                    <Button
                                                        variant="contained"
                                                        type="submit"
                                                        size="large"
                                                        disabled={formik.isSubmitting || !formik.isValid}
                                                    >
                                                        Send
                                                    </Button>
                                                    {!formik.isValid && Object.keys(formik.errors).filter(value => Object.keys(formik.touched).includes(value)).length > 0 &&
                                                        <FormHelperText error role="status">
                                                            Fix errors before sending.
                                                        </FormHelperText>
                                                    }
                                                    {formik.status===RESULT.FAILED &&
                                                        <FormHelperText error role="status">
                                                            Oops. An error occurred during submission.
                                                        </FormHelperText>
                                                    }
                                                </>
                                            }
                                        </Grid>
                                    </Grid>
                                </form>
                            }
                        </Formik>
                    </FormContainer>
                </Container>
            </Layout>
        </>
    );
}

export const getStaticProps: GetStaticProps<ContactProps> = async () => {
    const [profile] = await Promise.all([
        getProfile(),
    ]);
    return {
        props: {
            profile: profile.data,
        },
        revalidate: Number(process.env.REVALIDATE_INTERVAL),
    };
}