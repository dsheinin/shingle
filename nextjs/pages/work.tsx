import {getAllProjects, getProfile} from '../lib/api'
import { styled } from '@mui/material/styles';
import Layout from "../components/Layout";
import ProjectList from "../components/ProjectList";
import BlockHeading from "../components/BlockHeading";
import {
    Container, Grid, Typography, Chip, Box,
    Accordion, AccordionSummary, AccordionDetails,
    FormControlLabel, FormGroup, Checkbox, Button, Drawer,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import TuneIcon from '@mui/icons-material/Tune';
import {enableMapSet} from "immer";
import {useImmer} from "use-immer";
import { useState, useMemo } from "react";
import PageHeader from "../components/PageHeader";
import * as apiTypes from "../types/api";
import { GetStaticProps } from "next";
import HeadMetatags from "../components/HeadMetatags";

const FilterAccordion = styled(Accordion)(({ theme }) => ({
    backgroundColor: 'inherit',
    borderBottom: '1px solid',
    borderBottomColor: theme.palette.divider,
    boxShadow: 'none',
    '&:before': {
        display: 'none',
    },
    '&.Mui-expanded': {
        margin: 0,
    },
}));

type IsFilterSelectedFunction = (key: apiTypes.FilterKeys, id: number) => boolean;
type SetSelectedFilterFunction = (key: apiTypes.FilterKeys, id: number, value: boolean) => void;
type FiltersProps = {
    filters: apiTypes.ProjectFilters;
    isFilterSelected: IsFilterSelectedFunction;
    setSelectedFilter: SetSelectedFilterFunction;
};

function Filters({ filters, isFilterSelected, setSelectedFilter }: FiltersProps) {
    return (
        <aside id="project-filters" aria-label="Filters">
            <BlockHeading text="Filter"
                          icon={<TuneIcon/>} />
            <div>
                {(Object.keys(filters) as Array<keyof typeof filters>).map((key) =>
                    <FilterAccordion key={key}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls={`filter-${key}-content`}
                            id={`filter-${key}-header`}
                        >
                            <Typography component="h3" variant="h4">{filters[key].title}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <FormGroup>
                                {filters[key].items.map((item) =>
                                    <FormControlLabel
                                        key={item.id}
                                        control={<Checkbox name={key}
                                                           size="small"
                                                           color="primary"
                                                           value={item.id}
                                                           checked={isFilterSelected(key, item.id)}
                                                           onChange={e => setSelectedFilter(key, item.id, e.target.checked)} />}
                                        label={item.name}
                                    />
                                )}
                            </FormGroup>

                        </AccordionDetails>
                    </FilterAccordion>
                )}
            </div>
        </aside>
    );
}


const FilterChips = styled('div')(({ theme }) => ({
    paddingBottom: theme.spacing(2),
    borderBottom: `1px solid ${theme.palette.divider}`,
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(-0.5),
    '& > *': {
        margin: theme.spacing(0.5),
        display: 'inline-block',
    },
}));

const ProjectListContainer = styled('div')(({ theme }) => ({
    //marginTop: theme.spacing(4),
}));

const FilterGrid = styled(Grid)(({ theme }) => ({
    //marginTop: theme.spacing(4),
    [theme.breakpoints.down('md')]: {
        display: 'none',
    },
}));

const DrawerFilters = styled('div')(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        display: 'none',
    },
}));

type WorkProps = {
    projects: apiTypes.ListProject[];
    filters: apiTypes.ProjectFilters;
    profile: apiTypes.Profile;
};

export default function Work({ projects, filters, profile }: WorkProps) {
    const [filtersOpen, setFiltersOpen] = useState(false);

    enableMapSet();
    const [selectedFilters, updateSelectedFilters] = useImmer(
        (Object.keys(filters) as Array<keyof typeof filters>).reduce((selFil, key) => {
            selFil[key] =  new Set();
            return selFil;
        }, {} as {[key in apiTypes.FilterKeys]: Set<number>})
    );


    const isFilterSelected:IsFilterSelectedFunction = (key, id) => {
        return selectedFilters[key].has(id);
    };

    const setSelectedFilter:SetSelectedFilterFunction = (key, id, value) => {
        updateSelectedFilters(selectedFilters => {
            if (value) {
                selectedFilters[key].add(id);
            }
            else {
                selectedFilters[key].delete(id);
            }
        });
    };

    function clearAllFilters() {
        updateSelectedFilters(selectedFilters => {
            for (const key of (Object.keys(selectedFilters) as Array<keyof typeof selectedFilters>)) {
                selectedFilters[key].clear();
            }
        });
    }

    const filteredProjects = useMemo(() => {
        let fProjects = projects;
        for (let key of (Object.keys(selectedFilters) as Array<keyof typeof selectedFilters>)) {
            if (selectedFilters[key].size > 0) {
                fProjects = fProjects.filter(p => {
                    for (let id of p[key]){
                        if (selectedFilters[key].has(id)){
                            return true;
                        }
                    }
                    return false;
                });
            }
        }
        return fProjects;
    }, [selectedFilters, projects]);

    const selectedFiltersFlat: apiTypes.ProjectFilterItemFlat[] = [];
    for (const key of (Object.keys(filters) as Array<keyof typeof filters>)) {
        for (const item of filters[key].items) {
            if (isFilterSelected(key, item.id)) {
                selectedFiltersFlat.push({
                    key: key,
                    ...item,
                })
            }
        }
    }

    return (
        (<>
            <HeadMetatags
                title="Work"
                siteName={profile.name}
                description={profile.workIntro}
            />
            <Layout activeSection="work" profile={profile}>
                <Container>
                    <PageHeader heading="Work" />
                </Container>
                <Container component="main">
                    <Grid container spacing={4}>
                        <FilterGrid item xs={12} md={4} lg={3}>
                            <Filters
                                filters={filters}
                                isFilterSelected={isFilterSelected}
                                setSelectedFilter={setSelectedFilter}
                            />
                        </FilterGrid>
                        <Grid item xs={12} md={8} lg={9}>
                            <DrawerFilters>
                                <Button
                                    variant="contained"
                                    onClick={() => setFiltersOpen(true)}
                                    startIcon={<TuneIcon />}
                                    size="small"
                                    sx={{marginBottom: 2}}
                                >
                                    Filter
                                </Button>
                                <Drawer
                                    anchor="left"
                                    open={filtersOpen}
                                    onClose={() => setFiltersOpen(false)}
                                >
                                    <Filters
                                        filters={filters}
                                        isFilterSelected={isFilterSelected}
                                        setSelectedFilter={setSelectedFilter}
                                    />
                                    <Button
                                        variant="outlined"
                                        onClick={() => setFiltersOpen(false)}
                                        sx={{margin: 3}}
                                    >
                                        Show Results ({filteredProjects.length})
                                    </Button>
                                </Drawer>
                            </DrawerFilters>
                            {selectedFiltersFlat.length > 0 &&
                                <FilterChips>
                                    {selectedFiltersFlat.map(sf =>
                                        <span key={`${sf.key}-${sf.id}`}>
                                            <Chip label={sf.name}
                                                  variant="outlined"
                                                  onDelete={e => setSelectedFilter(sf.key, sf.id, false)}/>
                                        </span>
                                    )}
                                    {selectedFiltersFlat.length > 1 &&
                                    <span>
                                        <Chip label="CLEAR ALL"
                                              clickable
                                              color="primary"
                                              onClick={clearAllFilters}/>
                                    </span>
                                    }
                                </FilterChips>
                            }
                            <ProjectListContainer>
                                <Box sx={{marginBottom: 2}}>{filteredProjects.length} project{filteredProjects.length === 1 ? '' : 's'}</Box>
                                <div id="project-list">
                                    <ProjectList projects={filteredProjects}
                                                 selectedFiltersFlat={selectedFiltersFlat} />
                                </div>
                            </ProjectListContainer>
                        </Grid>
                    </Grid>
                </Container>
            </Layout>
        </>)
    );
}

export const getStaticProps: GetStaticProps<WorkProps> = async () => {
    const [projFil, profile] = await Promise.all([getAllProjects(), getProfile()]);
    return {
        props: {
            ...projFil.data,
            profile: profile.data,
        },
        revalidate: Number(process.env.REVALIDATE_INTERVAL),
    };
}