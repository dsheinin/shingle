import {getAllProjects, getProfile, getProject} from '../../lib/api'
import { styled } from '@mui/material/styles';
import Layout from "../../components/Layout";
import BlockHeading from "../../components/BlockHeading";
import {Container, Typography, Grid, Table, TableBody, TableRow, TableCell} from "@mui/material";
import LinkIcon from "@mui/icons-material/Link";
import PageHeader from "../../components/PageHeader";
import Image from 'next/image';
import Testimonials from "../../components/Testimonials";
import UnlaunchedTag from '../../components/UnlaunchedTag';
import * as apiTypes from "../../types/api";
import { GetStaticProps, GetStaticPaths } from 'next'
import HeadMetatags from "../../components/HeadMetatags";
import axios from 'axios';

const BodyImgContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    '& > figure': {
        width: '100%',
        margin: 0,
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        '.image-container-inner': {
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            border: `1px solid ${theme.palette.divider}`,
            background: theme.palette.grey['100'],
        },
        figcaption: {
            marginTop: theme.spacing(1),
            fontStyle: 'italic',
        }
    }
}));

type ProjectImageProps = {
    image: apiTypes.Image;
    caption?: string;
    altText?: string;
};

function ProjectImage({ image, caption, altText }: ProjectImageProps) {
    return (
        <BodyImgContainer>
            <figure>
                <div className="image-container-inner">
                    <Image src={image.url}
                           quality={100}
                           alt={altText}
                           width={image.width}
                           height={image.height} />
                </div>
                {caption &&
                    <Typography variant="caption"
                                component="figcaption"
                                dangerouslySetInnerHTML={{__html: caption}} />
                }
            </figure>
        </BodyImgContainer>
    );
}


const GutterGridItemRoot = styled(Grid)(({ theme }) => ({
    [theme.breakpoints.down('lg')]: {
        display: 'none',
    },
}));

function GutterGridItem() {
    return (
        <GutterGridItemRoot item lg={2} />
    );
}


const ProjectSectionRoot = styled('div')(({ theme }) => ({
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6),

    h2: {
        ...theme.typography.h3,
        textTransform: 'none',
    }
}));

type ProjectSectionProps = {
    text: string;
    image?: apiTypes.Image;
    caption?: string;
    imageAltText?: string;
};

function ProjectSection({ text, image, caption, imageAltText}: ProjectSectionProps) {
    const sideBySide = image && text;

    return (
        <ProjectSectionRoot>
            <Grid container rowSpacing={2} columnSpacing={4}>
                <GutterGridItem />
                <Grid item xs={12} sm={sideBySide ? 7 : 12}  md={9} lg={7}>
                    {image ?
                        <ProjectImage image={image} caption={caption} altText={imageAltText} /> :
                        <div dangerouslySetInnerHTML={{__html: text}}/>
                    }
                </Grid>
                {sideBySide &&
                    <Grid item xs={12} sm={5} md={3} lg={3} className="">
                        <div dangerouslySetInnerHTML={{__html: text}}/>
                    </Grid>
                }
            </Grid>
        </ProjectSectionRoot>
    );
}


const HighlightContainer = styled('div')(({ theme }) => ({
    border: '1px solid',
    borderColor: theme.palette.primary.main,
    marginBottom: theme.spacing(4),
}));

const HighlightBody = styled('div')(({ theme }) => ({
    fontSize: theme.typography.body2.fontSize,
    padding: theme.spacing(2),
    '& table': {
        '& tr:last-child': {
            '& td, & th': {
                borderBottom: 0,
            },
        },
        '& td, & th': {
            verticalAlign: 'top',
        },
        '& th': {
            width: '10%',
            textTransform: 'uppercase',
            fontWeight: 'bold',
            whiteSpace: 'nowrap',
        },
    },
}));

type ClientProps = {
    client: apiTypes.Client,
};

function Client({ client }: ClientProps) {
    return (
        <>
            {client.url ?
                <a target="_blank" rel="noreferrer" href={client.url}>{client.name}</a> :
                client.name
            }
        </>
    );
}

type ProjectDetailsProps = {
    project: apiTypes.DisplayProject;
};

function ProjectDetails({ project }: ProjectDetailsProps) {
    return (
        <Grid container spacing={2}>
            <GutterGridItem />
            <Grid item xs={12} md={9} lg={7}>
                <HighlightContainer>
                    <BlockHeading text="Project Details"
                                  color="primary" />
                    <HighlightBody>
                        <Table>
                            <TableBody>
                                {project.rolesDisplay.length > 0 &&
                                    <TableRow>
                                        <TableCell component="th" variant="head">My Role{project.rolesDisplay.length > 1 && 's'}</TableCell>
                                        <TableCell>{project.rolesDisplay.join(', ')}</TableCell>
                                    </TableRow>
                                }
                                {project.skillsDisplay.length > 0 &&
                                    <TableRow>
                                        <TableCell component="th" variant="head">Skill{project.skillsDisplay.length > 1 && 's'}</TableCell>
                                        <TableCell>{project.skillsDisplay.join(', ')}</TableCell>
                                    </TableRow>
                                }
                                {project.clientsDisplay.length > 0 &&
                                    <TableRow>
                                        <TableCell component="th" variant="head">Client{project.clientsDisplay.length > 1 && 's'}</TableCell>
                                        <TableCell>
                                            {
                                                project.clientsDisplay.map(client =>
                                                    <Client key={client.id} client={client} />
                                                ).reduce((prev, curr) => <>{[prev, ', ', curr]}</>)
                                            }
                                        </TableCell>
                                    </TableRow>
                                }
                                {project.domainsDisplay.length > 0 &&
                                    <TableRow>
                                        <TableCell component="th" variant="head">Domain{project.domainsDisplay.length > 1 && 's'}</TableCell>
                                        <TableCell>{project.domainsDisplay.join(', ')}</TableCell>
                                    </TableRow>
                                }
                                {project.startYear &&
                                    <TableRow>
                                        <TableCell component="th" variant="head">Dates</TableCell>
                                        <TableCell>
                                            {project.startYear}
                                            {project.endYear !== project.startYear &&
                                                <>
                                                    {project.endYear ?
                                                        <> - {project.endYear}</>:
                                                        <> - ongoing</>
                                                    }
                                                </>
                                            }
                                        </TableCell>
                                    </TableRow>
                                }
                                {project.simpleAttributes.map(att =>
                                    <TableRow key={att.id}>
                                        <TableCell component="th" variant="head">{att.name}</TableCell>
                                        <TableCell>{att.text}</TableCell>
                                    </TableRow>
                                )}
                                {project.links.length > 0 &&
                                    <TableRow>
                                        <TableCell component="th" variant="head"><LinkIcon titleAccess="Links"/></TableCell>
                                        <TableCell>
                                            {project.links.map(link =>
                                                <div key={link.id}><a target="_blank" rel="noreferrer" href={link.url}>{link.text}</a></div>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </HighlightBody>
                </HighlightContainer>
            </Grid>
        </Grid>
    );
}


const Lead = styled('div')(({ theme }) => ({
    fontSize: '1.1875rem',
    marginTop: theme.spacing(4),
}));

const MainTitle = styled(Typography)(({ theme }) => ({
    marginBottom: theme.spacing(4),
}));

type ProjectProps = {
    project: apiTypes.DisplayProject;
    profile: apiTypes.Profile;
};

export default function Project({ project, profile }: ProjectProps) {
    return (
        <>
            <HeadMetatags
                title={project.title}
                siteName={profile.name}
                description={project.summary}
                type="article"
                image={project.ogImage}
            />
            <Layout activeSection="work" profile={profile}>
                <Container>
                    <Grid container>
                        <GutterGridItem />
                        <Grid item xs={12} lg={7}>
                            <PageHeader heading={project.title } section={{name: "Work", href: "/work"}} />
                        </Grid>
                    </Grid>
                </Container>
                <Container component="main">
                    <ProjectDetails project={project} />
                    {(project.description || !project.launched) &&
                        <Grid container spacing={4}>
                            <GutterGridItem />
                            <Grid item xs={12} md={9} lg={7}>
                                {!project.launched &&
                                    <div><UnlaunchedTag /></div>
                                }
                                <Lead dangerouslySetInnerHTML={{__html: project.description}}/>
                            </Grid>
                        </Grid>
                    }
                    {project.sections.map(section =>
                        <ProjectSection key={section.id} {...section}/>
                    )}
                </Container>
                {project.testimonials.length > 0 &&
                    <Testimonials testimonials={project.testimonials} />
                }
            </Layout>
        </>
    );
}

export const getStaticPaths: GetStaticPaths = async () => {
    const projFil = await getAllProjects();
    const paths =  projFil.data.projects.map(project => {
        return {
            params: {
                slug: project.slug
            }
        };
    });

    return {
        paths,
        fallback: 'blocking'
    };
}

type ContextParams = {
    slug: string
}

export const getStaticProps: GetStaticProps<ProjectProps, ContextParams> = async ({ params }) => {
    const slug = params?.slug;
    if (slug) {
        try {
            const [project, profile] = await Promise.all([getProject(params?.slug), getProfile()]);
            const data = {
                project: project.data,
                profile: profile.data,
            }
            return {
                props: data,
                //revalidate: Number(process.env.REVALIDATE_INTERVAL),
            };
        } catch (error) {
            if (axios.isAxiosError(error) && error.response && error.response.status < 500) {
                return {
                    notFound: true,
                };
            }
            else {
                throw error ;
            }
        }
    }
    else {
        return {
            notFound: true,
        };
    }
}