import {getAllProjects} from "../../lib/api";

function urlForPath(path) {
    return `${process.env.BASE_URL}${process.env.NEXT_PUBLIC_BASE_PATH}${path}`;
}

function urlElement(path) {
    return `<url>
      <loc>${urlForPath(path)}</loc>
    </url>
    `;
}

export default async function handler(req, res) {

    res.statusCode = 200
    res.setHeader('Content-Type', 'text/xml')

    // Instructing the Vercel edge to cache the file
    //res.setHeader('Cache-control', 'stale-while-revalidate, s-maxage=3600')

    const projFil = await getAllProjects();

    // generate sitemap here
    const xml = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    ${urlElement('')} 
    ${urlElement('/work')} 
    ${urlElement('/about')} 
    ${urlElement('/contact')} 
    ${projFil.data.projects.map(project =>
        urlElement(`/work/${project.slug}`)
    ).join('')}
    </urlset>`

    res.end(xml)
}