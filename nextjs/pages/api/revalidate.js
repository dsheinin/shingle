export default async function handler(req, res) {
    // Check for secret to confirm this is a valid request
    if (req.body.token !== process.env.API_TOKEN) {
        return res.status(401).json({ message: 'Invalid token' });
    }

    const paths = JSON.parse(req.body.paths);

    // Run in sequence to ease server load a bit, pending a better solution from Next.js. See, e.g.
    // https://github.com/vercel/next.js/discussions/34567
    for (const path of paths) {
        try {
            console.log(`[Next.js] Revalidating: ${path}`);
            await res.unstable_revalidate(`${process.env.NEXT_PUBLIC_BASE_PATH}${path}`)
        } catch (err) {
            // If there was an error, Next.js will continue
            // to show the last successfully generated page
            console.log(`[Next.js] Validation failed`);
            return res.status(500).send(`Error revalidating: ${path}`);
        }
    }
    return res.status(200).send('Success!');

    // Alternative parallel implementation...
    /*
    try {
        await Promise.all(
            paths.map(async (path) => {
                console.log(`[Next.js] Revalidating: ${path}`);
                await res.unstable_revalidate(`${process.env.NEXT_PUBLIC_BASE_PATH}${path}`)
            })
        );
        return res.status(200).send('Success!');
    } catch (err) {
        // If there was an error, Next.js will continue
        // to show the last successfully generated page
        console.log(`[Next.js] Validation failed`);
        return res.status(500).send('Error revalidating');
    }
     */
}