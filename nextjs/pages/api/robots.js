export default async function handler(req, res) {
    res.statusCode = 200
    res.setHeader('Content-Type', 'text/plain')

    // Instructing the Vercel edge to cache the file
    //res.setHeader('Cache-control', 'stale-while-revalidate, s-maxage=3600')

    const text = `Sitemap: ${process.env.BASE_URL}${process.env.NEXT_PUBLIC_BASE_PATH}/sitemap.xml`

    res.end(text)
}