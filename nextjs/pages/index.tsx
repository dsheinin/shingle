import { styled } from '@mui/material/styles';
import {getFeaturedProjects, getFeaturedTestimonials, getProfile} from "../lib/api";
import Head from 'next/head'
import { Container, Typography, Button } from '@mui/material';
import Layout from "../components/Layout";
import Link from "next/link";
import Testimonials from "../components/Testimonials";
import Intro from "../components/Intro";
import FeaturedProjectList from "../components/FeaturedProjectList";
import * as apiTypes from "../types/api";
import { GetStaticProps } from "next";
import React from "react";
import HeadMetatags from "../components/HeadMetatags";

const ProjectsContainer = styled(Container)(({ theme }) => ({
    paddingTop: theme.spacing(7),
    paddingBottom: theme.spacing(4),
    '& h2': {
        marginBottom: theme.spacing(4),
    },
}));

const ProjectsButtons = styled('div')(({ theme }) => ({
    textAlign: 'center',
    marginTop: theme.spacing(6),
}));

type HomeProps = {
    projects: apiTypes.ListProject[];
    profile: apiTypes.Profile;
    testimonials: apiTypes.Testimonial[];
}

export default function Home({ projects, profile, testimonials }: HomeProps) {
    return (
        (<>
            <HeadMetatags
                siteName={profile.name}
                description={profile.homeIntroPlain}
            />
            <Layout profile={profile}>
                <main>
                    <Intro text={profile.homeIntro} heading={profile.name} />
                    {projects.length > 0 &&
                        <ProjectsContainer>
                            <Typography variant="h3" component="h2" color="text.secondary">Featured Work</Typography>
                            <FeaturedProjectList projects={projects} />
                            <ProjectsButtons>
                                <Link href="/work" passHref>
                                    <Button variant="contained" size="large">See more work</Button>
                                </Link>
                            </ProjectsButtons>
                        </ProjectsContainer>
                    }
                    <Testimonials testimonials={testimonials} />
                </main>
            </Layout>
        </>)
    );
}

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
    const [projects, profile, testimonials] = await Promise.all([
        getFeaturedProjects(),
        getProfile(),
        getFeaturedTestimonials(),
    ]);
    return {
        props: {
            projects: projects.data,
            profile: profile.data,
            testimonials: testimonials.data,
        },
        revalidate: Number(process.env.REVALIDATE_INTERVAL),
    };
}