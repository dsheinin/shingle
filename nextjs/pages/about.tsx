import Layout from "../components/Layout";
import Intro from "../components/Intro";
import {getAboutSections, getProfile} from "../lib/api";
import {Container, Typography, Grid} from '@mui/material';
import { styled } from '@mui/material/styles';
import PageHeader from "../components/PageHeader";
import * as apiTypes from "../types/api";
import {GetStaticProps} from "next";
import HeadMetatags from "../components/HeadMetatags";

const AboutMain = styled('main')(({ theme }) => ({
    paddingBottom: theme.spacing(4),
}));

const SectionGrid = styled(Grid)(({ theme }) => ({
    '&:not(:last-child)': {
        marginBottom: theme.spacing(4),
    },
}));

const SectionText = styled('div')(({ theme }) => ({
    '& h3': {
        ...theme.typography.h3,
        fontSize: '1em',
        margin: 0,
    }
}));

const Section = styled('div')(({ theme }) => ({
    backgroundColor: theme.palette.secondary.dark,
    padding: `${theme.spacing(4)} ${theme.spacing(8)}`,
    fontSize: '1.625rem',
    '& h2': {
        textTransform: 'uppercase',
        marginBottom: theme.spacing(4),
        textAlign: 'center',
    },
    [theme.breakpoints.down('sm')]: {
        fontSize: '1.25rem',
        padding: `${theme.spacing(2)} ${theme.spacing(4)}`,
    },
}));

const SectionIconContainer = styled('div')(({ theme }) => ({
    '& svg': {
        display: 'block',
        margin: 'auto',
        width: 72,
        height: 72,
        fill: theme.palette.secondary.light,
        marginBottom: theme.spacing(2),
    },
}));

type AboutProps = {
    profile: apiTypes.Profile;
    sections: apiTypes.AboutSection[];
};

export default function About({ profile, sections }: AboutProps) {
    return (
        <>
            <HeadMetatags
                title="About"
                siteName={profile.name}
                description={profile.aboutIntroPlain}
            />
            <Layout profile={profile} activeSection="about" dark>
                <Container>
                    <PageHeader heading="About" />
                </Container>
                <AboutMain>
                    <Intro text={profile.aboutIntro} />
                    <Container>
                        {sections.map(section =>
                            <SectionGrid key={section.id} container justifyContent="center">
                                <Grid item xs={12} md={10} lg={8}>
                                    <Section>
                                        {section.svgIcon &&
                                        <SectionIconContainer dangerouslySetInnerHTML={{__html: section.svgIcon}} />
                                        }
                                        <Typography variant="h2">{section.title}</Typography>
                                        <SectionText dangerouslySetInnerHTML={{__html: section.text}} />
                                    </Section>
                                </Grid>
                            </SectionGrid>
                        )}
                    </Container>
                </AboutMain>
            </Layout>
        </>
    );
}

export const getStaticProps: GetStaticProps<AboutProps> = async () => {
    const [profile, sections] = await Promise.all([
        getProfile(),
        getAboutSections(),
    ]);
    return {
        props: {
            profile: profile.data,
            sections: sections.data,
        },
        revalidate: Number(process.env.REVALIDATE_INTERVAL),
    };
}