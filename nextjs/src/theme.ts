import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { CSSProperties } from "@mui/material/styles/createMixins";

const fontFamilySecondary = 'Montserrat, sans-serif';

const colorSchemes = {
    'canuck': {
        primary: {
            main: '#008531',
            light: '#00a63c',
            contrastText: '#fff',
        },
        secondary: {
            light: '#8fcad6',
            main: '#00639b',
            dark: '#0b547b',
            lowerContrastText: '#d6ebff',
            contrastText: '#fff',
            imageScreen: '#8fcad6',
        },
    },
    'mo': {
        primary: {
            main: '#00819c',
            light: '#4ca6b9',
            contrastText: '#fff',
        },
        secondary: {
            light: '#cf366f',
            main: '#8c0132',
            dark: '#6d0619',
            lowerContrastText: '#ddb5c3',
            contrastText: '#fff',
            imageScreen: '#8fcad6',
            },
    }
}
const colorScheme = colorSchemes[process.env.NEXT_PUBLIC_COLOR_SCHEME as "canuck" | "mo"];

// Augment TS modules for custom theme

declare module '@mui/material/styles' {
    interface TypographyVariants {
        fontFamilySecondary: string;
    }

    // allow configuration using `createTheme`
    interface TypographyVariantsOptions {
        fontFamilySecondary?: string;
    }

    interface PaletteColor {
        lowerContrastText: string;
    }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
    interface TypographyPropsVariantOverrides {
        fontFamilySecondary: true;
    }
}

declare module "@mui/material/styles/createMixins" {
    // Allow for custom mixins to be added
    interface Mixins {
        light?: CSSProperties;
        dark?: CSSProperties;
    }
}


let theme = createTheme({
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                'html, body, #__next': {
                    height: '100%',
                },
                '#__next': {
                    display: 'flex',
                    flexDirection: 'column',
                },
                'p, ul': {
                    margin: 0,
                    marginBottom: '1em',
                    '&:last-child': {
                        marginBottom: 0,
                    }
                },
                img: {
                    maxWidth: '100%',
                    height: 'auto',
                },
            },
        },
        MuiButton: {
            defaultProps: {
                disableElevation: true,
            },
        },
    },
    palette: {
        primary: {
            ...colorScheme.primary,
        },
        secondary: {
            ...colorScheme.secondary,
        },
        text: {
            primary: '#455a64',
            secondary: '#757575',
        },
        background: {
            default: '#fff',
        },
    },
    typography: {
        fontFamily: 'Hind Siliguri, sans-serif',
        fontFamilySecondary: fontFamilySecondary,
        h1: {
            fontFamily: fontFamilySecondary,
            fontSize: '2.5rem',
            fontWeight: 900,
            textTransform: 'uppercase',
        },
        h2: {
            fontFamily: fontFamilySecondary,
            fontSize: '2rem',
            fontWeight: 900,
        },
        h3: {
            fontSize: '1.5rem',
            fontWeight: 'bold',
            textTransform: 'uppercase',
        },
        h4: {
            fontSize: '0.9375rem',
            fontWeight: 'bold',
            textTransform: 'uppercase',
        },
        body2: {
            fontSize: '0.9375rem',
        },
        caption: {
            fontSize: '0.875rem',
        },
    },
    mixins: {
        light: {
            'a:not(.MuiButton-root)': {
                color: colorScheme.primary.main,
                textDecoration: 'none',
                '&:hover, &:focus': {
                    textDecoration: 'underline',
                },
            },
        },
        dark: {
            backgroundColor: colorScheme.secondary.main,
            color: colorScheme.secondary.contrastText,
            'a:not(.MuiButton-root)': {
                color: colorScheme.secondary.light,
                textDecoration: 'none',
                '&:hover, &:focus': {
                    textDecoration: 'underline',
                },
            },
        }
    }
});

theme = responsiveFontSizes(theme);

export default theme;