// TODO: auto-generate API types, e.g. using OpenAPI from DRF

export interface Profile {
    name: string;
    nameHeader: boolean;
    title: string;
    phoneDisplay: string;
    email: string;
    location: string;
    svgLogo: string;
    favicon: string;
    homeIntro: string;
    homeIntroPlain: string;
    aboutIntro: string;
    aboutIntroPlain: string;
    workIntro: string;
    contactIntro: string;
    footer: string;
}

export interface AboutSection {
    id: number;
    title: string;
    svgIcon: string;
    text: string;
    rank: number;
}

export interface Image {
    url: string;
    width: number;
    height: number;
}

export interface Project {
    id: number;
    slug: string;
    title: string;
    summary: string;
    clients: number[];
    domains: number[];
    skills: number[];
    roles: number[];
    launched: boolean;
}

export interface ListProject extends Project {
    thumbnail: Image;
}

export interface Client {
    id: number;
    name: string;
    url: string;
}

export interface ProjectLink {
    id: number;
    url: string;
    text: string;
}

export interface ProjectSection {
    id: number;
    image: Image;
    imageAltText: string;
    caption: string;
    text: string;
}

export interface ProjectSimpleAttribute {
    id: number;
    name: string;
    text: string;
}

export interface DisplayProject extends Project {
    leadImage: Image;
    ogImage: Image;
    description: string;
    domainsDisplay: string[];
    rolesDisplay: string[];
    skillsDisplay: string[];
    clientsDisplay: Client[];
    links: ProjectLink[];
    sections: ProjectSection[];
    testimonials: Testimonial[];
    startYear: number;
    endYear: number;
    simpleAttributes: ProjectSimpleAttribute[];
}

export interface Testimonial {
    id: number;
    text: string;
    name: string;
    role: string;
    clientDisplay: string;
}

export interface ProjectFilterItem {
    id: number;
    name: string;
    projectCount: number;
}

export type FilterKeys = "clients" | "domains" | "skills" | "roles";

export interface ProjectFilterItemFlat extends ProjectFilterItem {
    key: FilterKeys;
}

export interface ProjectFilter {
    title: string;
    items: ProjectFilterItem[];
}

export type ProjectFilters = {
    [key in FilterKeys]: ProjectFilter;
}