module.exports = {
    basePath: process.env.NEXT_PUBLIC_BASE_PATH,
    images: {
        domains: [new URL(process.env.NEXT_PUBLIC_API_BASE_URL).hostname],
    },
    async rewrites() {
        return [
            {
                source: '/sitemap.xml',
                destination: '/api/sitemap',
            },
            {
                source: '/robots.txt',
                destination: '/api/robots',
            },
        ];
    },
}