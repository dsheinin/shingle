import { render, within, fireEvent } from "@testing-library/react";
import Work from "../../pages/work";

describe('Work', () => {
    test('test filters', () => {
        render(<Work
            projects={[
                {"id": 1, "skills": [1], "roles": [1]},
                {"id": 2, "skills": [2], "roles": [1]},
                {"id": 3, "skills": [1], "roles": [2]},
                {"id": 4, "skills": [2], "roles": [2]},
            ]}
            filters={{
                "roles": {"items": [{"id": 1}, {"id": 2}]},
                "skills": {"items": [{"id": 1}, {"id": 2}]},
            }}
            profile={{}}
        />);

        const filterContainer = document.getElementById('project-filters');
        const filters = within(filterContainer).getAllByRole('checkbox', { hidden: true });
        expect(filters.length).toBe(4);

        const projectContainer = document.getElementById('project-list');
        let projects = within(projectContainer).getAllByRole('link');
        expect(projects.length).toBe(4);

        // Filters from different filter types should be "AND"ed. Selecting 1 role and 1 skill filter should limit to
        // the only project that matches the role AND the skill.
        fireEvent.click(filters[0]);
        fireEvent.click(filters[2]);
        projects = within(projectContainer).getAllByRole('link');
        expect(projects.length).toBe(1);

        // Filters from the same filter type should be "OR"ed.  Selecting another role filter should limit to projects
        // that match (role 1 OR role 2) AND the skill
        fireEvent.click(filters[1]);
        projects = within(projectContainer).getAllByRole('link');
        expect(projects.length).toBe(2);
    });
});