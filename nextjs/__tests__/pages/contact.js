import { render, within, fireEvent } from "@testing-library/react";
import Contact from "../../pages/contact";
import { act } from "react-dom/test-utils";
import axios from 'axios';

jest.mock('axios');

describe('Contact', () => {
    test('test form submission', async () => {
        let result;

        // NOTE: Formik throws misleading warnings when testing under some circumstances. await act() calls below
        // are workarounds. See:
        // https://github.com/jaredpalmer/formik/issues/3418#issuecomment-985782161
        // https://github.com/jaredpalmer/formik/issues/1543#issuecomment-547501926

        await act(async () => {
            result = render(<Contact profile={{}}/>);
        });

        let contactForm = document.getElementById('contact-form');
        let submitButton = within(contactForm).getByRole('button');
        let nameField = within(contactForm).getByLabelText(/name/i);
        let emailField = within(contactForm).getByLabelText(/email/i);
        let messageField = within(contactForm).getByLabelText(/message/i);

        // Submit button should be initially disabled
        expect(submitButton).toBeDisabled();

        // Check for still-disabled button with missing fields. No global error message because all touched fields
        // are error-free.
        // (Simulate focus/blur because touched status is updated on blur.)
        await act(async () => {
            fireEvent.focus(nameField);
            fireEvent.change(nameField, {target: {value: 'A Name'}});
            fireEvent.focus(messageField);
            fireEvent.change(messageField, {target: {value: 'The message.'}});
            fireEvent.blur(messageField);
        });
        expect(within(contactForm).queryByText(/fix errors/i)).not.toBeInTheDocument();
        expect(submitButton).toBeDisabled();

        // Touching email field should add global error message.
        await act(async () => {
            fireEvent.focus(emailField);
            fireEvent.blur(emailField);
        });
        expect(within(contactForm).getByText(/fix errors/i)).toBeInTheDocument();
        expect(submitButton).toBeDisabled();

        // Adding an invalid email address shouldn't change things.
        await act(async () => {
            fireEvent.focus(emailField);
            fireEvent.change(emailField, {target: {value: 'invalidemail'}});
            fireEvent.blur(emailField);
        });
        expect(within(contactForm).getByText(/fix errors/i)).toBeInTheDocument();
        expect(submitButton).toBeDisabled();

        // Adding a valid email address should remove global message and enable button.
        await act(async () => {
            fireEvent.focus(emailField);
            fireEvent.change(emailField, {target: {value: 'test@example.com'}});
            fireEvent.blur(emailField);
        });
        expect(within(contactForm).queryByText(/fix errors before sending/i)).not.toBeInTheDocument();
        expect(submitButton).toBeEnabled();

        // Unsuccessful submission
        axios.mockRejectedValueOnce(new Error('test error'));
        await act(async () => {
            fireEvent.click(submitButton);
        });
        expect(axios).toHaveBeenCalled();
        expect(within(contactForm).getByText(/an error occurred/i)).toBeInTheDocument();

        // Successful submission: fields disabled, button removed and success messge
        axios.mockResolvedValueOnce({});
        await act(async () => {
            fireEvent.click(submitButton);
        });
        expect(axios).toHaveBeenCalled();
        expect(nameField).toBeDisabled();
        expect(emailField).toBeDisabled();
        expect(messageField).toBeDisabled();
        expect(within(contactForm).queryByRole('button')).not.toBeInTheDocument();
    });
});
