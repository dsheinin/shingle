import { AppBar, Container, Toolbar } from "@mui/material";
import Link from 'next/link'
import { styled } from '@mui/material/styles';
import ExpandableMenu from "./ExpandableMenu";
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import MailOutlinedIcon from '@mui/icons-material/MailOutlined';
import Head from "next/head";
import React from "react";
import * as apiTypes from "../types/api";

const HeaderAppBar = styled(AppBar)(({ theme }) => ({
    borderBottom: `1px solid ${theme.palette.grey['400']}`,
}));

interface ContentProps {
    dark?: boolean;
}

const Content = styled('div', {
    shouldForwardProp: (prop) => prop !== "dark"
})<ContentProps>(({ theme, dark }) => ({
    flex: '1 0 auto',
    ...(dark ? theme.mixins.dark : theme.mixins.light),
}));

const HeaderToolbar = styled(Toolbar)(({ theme }) => ({
    minHeight: 85,
    justifyContent: 'space-between',
    '& nav': {
        '& a, & a:visited': {
            color: theme.palette.secondary.lowerContrastText,
            fontWeight: 'bold',
            fontSize: '1rem',
            textTransform: 'uppercase',
            textDecoration: 'none',
            marginLeft: theme.spacing(5),

            '&:hover, &:focus, &.active': {
                color: theme.palette.secondary.contrastText,
                textDecoration: 'none',
                borderBottom: '2px solid',
            }
        },
    },
}));

const Brand = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    margin: `${theme.spacing(2)} 0`,
    '.name': {
        fontWeight: 'bold',
    },
    '.title': {
        fontSize: '1.25rem',
    },
    '.name ~ .title': {
        fontSize: '1rem',
    }
}));

const LogoContainer = styled('a')(({ theme }) => ({
    display: 'flex',
    '& svg': {
        height: 30,
        fill: theme.palette.secondary.contrastText,
        stroke: 'transparent',
    },
}));

const SiteTitle = styled('a')(({ theme }) => ({
    //[theme.breakpoints.down('md')]: {
    //    display: 'none',
    //},
    paddingLeft: theme.spacing(2),
    borderLeft: '1px solid',
    marginLeft: theme.spacing(2),
    lineHeight: 1.1,
    'a&, a&:visited, a&:hover, a&:focus': {
        color: theme.palette.secondary.lowerContrastText,
        textDecoration: 'none',
    }
}));

const MainNav = styled('nav')(({ theme }) => ({
    marginLeft: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
        display: 'none',
    },
    '.MuiSvgIcon-root': {
        verticalAlign: 'sub',
    },
}));

const ResponsiveNav = styled(ExpandableMenu)(({ theme }) => ({
    marginLeft: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
        display: 'none',
    },
}));

const Footer = styled('footer')(({ theme }) => ({
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    ...theme.mixins.dark,
    background: theme.palette.grey['900'],
    '& > .MuiContainer-root': {
        textAlign: 'center',
    }
}));

type LayoutProps = {
    profile: apiTypes.Profile;
    activeSection?: string;
    children: React.ReactNode;
    dark?: boolean;
};

export default function Layout({ profile, activeSection, children, dark }: LayoutProps) {
    return (
        <>
            {profile.favicon &&
                <Head>
                    <link rel="shortcut icon" href={profile.favicon} />
                </Head>
            }
            <HeaderAppBar color="secondary" position="static" elevation={0}>
                <Container>
                    <HeaderToolbar disableGutters>
                        <Brand>
                            <Link href="/" passHref>
                                <LogoContainer dangerouslySetInnerHTML={{__html: profile.svgLogo}} />
                            </Link>
                            <Link href="/" passHref>
                                <SiteTitle>
                                    {profile.nameHeader &&
                                        <div className="name">{profile.name}</div>
                                    }
                                    <div className="title">{profile.title}</div>
                                </SiteTitle>
                            </Link>
                        </Brand>
                        <MainNav>
                            <Link href="/work">
                                <a className={activeSection === 'work' ? 'active' : ''}><WorkOutlineOutlinedIcon fontSize="small" /> Work</a>
                            </Link>
                            <Link href="/about">
                                <a className={activeSection === 'about' ? 'active' : ''}><InfoOutlinedIcon fontSize="small" /> About</a>
                            </Link>
                            <Link href="/contact">
                                <a className={activeSection === 'contact' ? 'active' : ''}><MailOutlinedIcon fontSize="small" /> Contact</a>
                            </Link>
                        </MainNav>
                        <ResponsiveNav activeSection={activeSection} />
                    </HeaderToolbar>
                </Container>
            </HeaderAppBar>
            <Content dark={dark}>
                { children }
            </Content>
            {profile.footer &&
                <Footer>
                    <Container dangerouslySetInnerHTML={{__html: profile.footer}}>
                    </Container>
                </Footer>
            }
        </>
    );
}