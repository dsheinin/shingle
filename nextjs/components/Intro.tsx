import { styled } from '@mui/material/styles';
import { Container, Typography } from "@mui/material";

const Root = styled('div')(({ theme }) => ({
    ...theme.mixins.dark,
    fontFamily: theme.typography.fontFamilySecondary,
    fontSize: '2.25rem',
    [theme.breakpoints.down('md')]: {
        fontSize: '2rem',
    },
    [theme.breakpoints.down('sm')]: {
        fontSize: '1.5rem',
    },
    textAlign: 'center',
    padding: theme.spacing(8, 4),
    '& strong, & b': {
        fontWeight: 900,
    },
}));

type IntroProps = {
    text: string;
    heading?: string;
};

export default function Intro({ text, heading }: IntroProps) {
    if (text) {
        return (
            <Root>
                <Container>
                    {heading &&
                        <Typography variant="h1">{heading}</Typography>
                    }
                    <div dangerouslySetInnerHTML={{__html: text}} />
                </Container>
            </Root>
        );
    }
    else {
        return null;
    }
}