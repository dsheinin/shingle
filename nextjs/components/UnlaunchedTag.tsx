import { styled } from "@mui/material/styles";

const Root = styled('span')(({ theme }) => ({
    ...theme.mixins.dark,
    padding: '0 19px',
    border: '1px solid',
    fontWeight: 'bold',
    display: 'inline-block',
}));

export default function UnlaunchedTag() {
    return (
        <Root>Launching soon!</Root>
    );
}