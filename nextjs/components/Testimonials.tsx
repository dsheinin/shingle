import {Container, Grid} from "@mui/material";
import { styled } from '@mui/material/styles';
import * as apiTypes from "../types/api";

const TestimonialContainer = styled('div')(({ theme }) => ({
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6),
    fontSize: '1.125rem',
    backgroundImage: `url(${process.env.NEXT_PUBLIC_BASE_PATH}/quote.svg)`,
    backgroundRepeat: 'no-repeat',
    paddingTop: 40,
    [theme.breakpoints.up('sm')]: {
        paddingTop: 0,
        paddingLeft: 80,
    },
    '&:not(:last-child) .testimonial': {
        paddingBottom: theme.spacing(6),
        borderBottom: `1px solid ${theme.palette.grey['400']}`,
    },
}));

const TestimonialCredits = styled('div')(({ theme }) => ({
    marginTop: theme.spacing(4),
    fontSize: theme.typography.caption.fontSize,
    paddingLeft: theme.spacing(6),
}));

const TestimonialName = styled('div')(({ theme }) => ({
    fontWeight: 'bold',
}));

type TestimonialProps = {
    testimonial: apiTypes.Testimonial;
}

function Testimonial({ testimonial }: TestimonialProps) {
    return (
        <TestimonialContainer>
            <div className="testimonial">
                <div>{testimonial.text}</div>
                <TestimonialCredits>
                    <TestimonialName>{testimonial.name}</TestimonialName>
                    {(testimonial.role || testimonial.clientDisplay) &&
                    <div>{testimonial.role}{testimonial.role && testimonial.clientDisplay && <>, </>}{testimonial.clientDisplay}</div>
                    }
                </TestimonialCredits>
            </div>
        </TestimonialContainer>
    );
}

const TestimonialSection = styled('div')(({ theme }) => ({
    backgroundImage: `url(${process.env.NEXT_PUBLIC_BASE_PATH}/waves.svg)`,
    backgroundSize: '100% 400px',
    backgroundRepeat: 'no-repeat',
    backgroundColor: '#1C303A',
    paddingTop: 200,
    paddingBottom: theme.spacing(4),
    color: '#fff',
    [theme.breakpoints.between('md', 'lg')]: {
        backgroundSize: '100% 300px',
        paddingTop: 150,
    },
    [theme.breakpoints.between('sm', 'md')]: {
        backgroundSize: '100% 200px',
        paddingTop: 125,
    },
    [theme.breakpoints.down('sm')]: {
        backgroundSize: '100% 100px',
        paddingTop: 75,
    },
}));

type TestimonialsProps = {
    testimonials: apiTypes.Testimonial[];
}

export default function Testimonials({ testimonials }: TestimonialsProps) {
    return (
        <TestimonialSection>
            <Container>
                <Grid container justifyContent="center">
                    <Grid item md={8} lg={7}>
                        {testimonials.map((testimonial, idx) =>
                            <Testimonial testimonial={testimonial} key={testimonial.id} />
                        )}
                    </Grid>
                </Grid>
            </Container>
        </TestimonialSection>
    );
}