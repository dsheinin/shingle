import { Container, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import Link from 'next/link'
import { Section } from "../types"

const Root = styled('div')(({ theme }) => ({
    padding: `${theme.spacing(3)} 0`,
}));

const SectionLink = styled('a')(({ theme }) => ({
    fontSize: '1.25rem',
    fontWeight: 'bold',
    lineHeight: 1,
    textTransform: 'uppercase',
}));

type PageHeaderProps = {
    heading: string;
    section?: Section;
};

export default function PageHeader({ heading, section }: PageHeaderProps) {
    return (
        <Root>
            {section &&
                <Link href={section.href} passHref>
                    <SectionLink>{section.name}</SectionLink>
                </Link>
            }
            <Typography variant="h1">{heading}</Typography>
        </Root>
    );
}