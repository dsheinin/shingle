import Link from "next/link";
import { Card, CardActionArea, CardContent, Typography } from "@mui/material";
import Image from "next/image";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { styled } from "@mui/material/styles";
import * as apiTypes from "../types/api";

const FeatureProjectsContainer = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginTop: -32,
    marginLeft: -32,
    '& > *': {
        paddingTop: 32,
        paddingLeft: 32,
    },
}));

const FeatureProjectCard = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.primary.main,
    borderWidth: 5,
    borderRadius: 20,
    maxWidth: 362,
    'a.MuiCardActionArea-root': {
        color: 'white',
        padding: theme.spacing(2),
        transition: theme.transitions.create(['background-color'], {
            duration: theme.transitions.duration.standard,
        }),
        '.thumbnail-container': {
            position: 'relative',
            paddingBottom: '66.7%',
            marginBottom: theme.spacing(2),
            width: '100%',
            border: '1px solid',
            '& > .inner': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                background: theme.palette.grey[300],
            }
        },
        '.arrow-container': {
            width: '100%',
            padding: `4px ${theme.spacing(2)}`,
            textAlign: 'right',
            svg: {
                position: 'relative',
                right: '50%',
                transform: 'translateX(50%)',
                transition: theme.transitions.create(['transform', 'right'], {
                    duration: theme.transitions.duration.standard,
                }),
            }
        },
        '&:hover, &:focus': {
            textDecoration: 'none',
            backgroundColor: theme.palette.primary.dark,
            '.arrow-container svg': {
                right: 0,
                transform: 'translateX(0)',
            },
        },
    },
}));

type FeaturedProjectListProps = {
    projects: apiTypes.ListProject[];
};

export default function FeaturedProjectList({ projects }: FeaturedProjectListProps) {
    return (
        <FeatureProjectsContainer>
            {projects.map(project =>
                <div key={project.id}>
                    <FeatureProjectCard sx={{height: '100%'}}>
                        <Link href={`/work/${project.slug}`} passHref>
                            <CardActionArea sx={{height: '100%', display: 'flex', flexDirection: 'column'}}>
                                <CardContent sx={{flexGrow: 1}}>
                                    <div className="thumbnail-container">
                                        <div className="inner">
                                            {project.thumbnail &&
                                                <Image
                                                    layout="responsive"
                                                    src={project.thumbnail.url}
                                                    alt="project thumbnail"
                                                    width={project.thumbnail.width}
                                                    height={project.thumbnail.height}
                                                />
                                            }
                                        </div>
                                    </div>
                                    <Typography variant="h2" component="div" gutterBottom>
                                        {project.title}
                                    </Typography>
                                    <Typography>{project.summary}</Typography>
                                </CardContent>
                                <div className="arrow-container">
                                    <Typography variant="h2" component="span">
                                        <ArrowForwardIcon sx={{fontSize: '2.5em'}}/>
                                    </Typography>
                                </div>
                            </CardActionArea>
                        </Link>
                    </FeatureProjectCard>
                </div>
            )}
        </FeatureProjectsContainer>
    );
}