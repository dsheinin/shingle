import { Typography } from "@mui/material";
import { styled } from '@mui/material/styles';
import { SvgIconComponent} from "@mui/icons-material";
import React from "react";

interface StyledTypographyProps {
    color?: string;
}

const StyledTypography = styled(Typography, {
    shouldForwardProp: (prop) => prop !== "color"
})<StyledTypographyProps>(({ theme, color }) => ({
    backgroundColor: color === "primary" ? theme.palette.primary.main : '#455a65',
    color: '#fff',
    padding: `${theme.spacing(1)} ${theme.spacing(2)}`,
    display: 'flex',
    alignItems: 'center',
    fontSize: '1rem',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    '& svg': {
        marginRight: theme.spacing(1),
    },
})) as typeof Typography;

type BlockHeadingProps = {
    text: string;
    color?: "primary";  // TODO: support other values or make this boolean
    icon?: JSX.Element;
    component?: React.ElementType;
};

export default function BlockHeading({ text, color, icon, component }: BlockHeadingProps) {
    return (
        <StyledTypography component={component || 'h2'} color={color}>
            <>{icon} {text}</>
        </StyledTypography>
    );
}