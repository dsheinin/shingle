import { Typography, Grid, Container } from "@mui/material";
import Link from "next/link";
import Image from 'next/image'
import { styled } from "@mui/material/styles";
import TuneIcon from '@mui/icons-material/Tune';
import UnlaunchedTag from './UnlaunchedTag';
import * as apiTypes from "../types/api";

interface ProjectLinkProps {
    unlaunched?: boolean;
}

const ProjectLink = styled('a', {
    shouldForwardProp: (prop) => prop !== "unlaunched"
})<ProjectLinkProps>(({ theme, unlaunched }) => ({
    display: 'block',
    marginBottom: theme.spacing(6),
    '.summary': {
        color: theme.palette.text.primary,
        display: 'inline-block',
        width: '100%',
    },
    '.filters': {
        marginTop: theme.spacing(1),
        fontWeight: 'bold',
        display: 'flex',
        //alignItems: 'center',
        fontSize: '0.875rem',
        '.MuiSvgIcon-root': {
            marginRight: theme.spacing(1),
        }
    },
    '.image-grid': {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        '.image-container': {
            width: '100%',
            position: 'relative',
            border: `1px solid ${theme.palette.divider}`,
            background: '#ccc',
            [theme.breakpoints.down('sm')]: {
                maxWidth: 400,
                marginBottom: -40,
            },
            img: {
                filter: `grayscale(0.5)${unlaunched ? ' blur(3px)' : ''}`,
                transition: theme.transitions.create(['transform']),
            },
        },
    },
    '.text-grid': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '.text-container': {
            width: '100%',
            padding: 20,
            zIndex: 5,
            background: 'rgba(235, 235, 235, 0.9)',
            [theme.breakpoints.up('sm')]: {
                marginLeft: -40,
                width: 'calc(100% + 40px)',
            },
            [theme.breakpoints.down('sm')]: {
                maxWidth: 400,
            },
            '.project-title': {
                marginBottom: '0.5rem',
                lineHeight: 1.1,
            },
            '.unlaunched-container': {
                marginLeft: -20,
                marginTop: -30,
                marginBottom: 10,
            }
        },
    },
    '&:hover, &:focus, &:active': {
        '.image-container': {
            position: 'relative',
            img: {
                filter: unlaunched ? 'blur(3px)' : 'none',
            },
        },
    },
}));

type ProjectInListProps = {
    project: apiTypes.ListProject;
    selectedFiltersFlat: apiTypes.ProjectFilterItemFlat[];
};

function ProjectInList({ project, selectedFiltersFlat }: ProjectInListProps) {
    return (
        <Link href={`/work/${project.slug}`} passHref>
            <ProjectLink unlaunched={!project.launched}>
                <Grid container>
                    <Grid item xs={12} sm={5} className="image-grid">
                        {project.thumbnail &&
                            <div className="image-container">
                                <Image
                                    layout="responsive"
                                    src={project.thumbnail.url}
                                    alt="project thumbnail"
                                    width={project.thumbnail.width}
                                    height={project.thumbnail.height}
                                />
                            </div>
                        }
                    </Grid>
                    <Grid item xs={12} sm={7} className="text-grid">
                        <div className="text-container">
                            {!project.launched &&
                                <div className="unlaunched-container"><UnlaunchedTag /></div>
                            }
                            <Typography variant="h2" component="div" className="project-title">
                                {project.title}
                            </Typography>
                            <Typography component="div" className="summary">
                                {project.summary}
                                {selectedFiltersFlat && selectedFiltersFlat.length > 0 &&
                                    <div className="filters">
                                        <TuneIcon fontSize="small"/> {selectedFiltersFlat.filter(filter => project[filter.key].includes(filter.id)).map(filter => filter.name).join(', ')}...
                                    </div>
                                }
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </ProjectLink>
        </Link>
    );
}

type ProjectListProps = {
    projects: apiTypes.ListProject[];
    selectedFiltersFlat: apiTypes.ProjectFilterItemFlat[];
};

export default function ProjectList({ projects, selectedFiltersFlat }: ProjectListProps) {
    return (
        (<div>
            {projects.map((project) =>
                <ProjectInList project={project}
                               selectedFiltersFlat={selectedFiltersFlat}
                               key={project.id} />
            )}
        </div>)
    );
}