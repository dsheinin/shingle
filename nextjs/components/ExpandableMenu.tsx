import { ListItemText, IconButton, Menu, MenuItem, ListItemIcon } from "@mui/material";
import { useState } from "react";
import Link from 'next/link'
import MenuIcon from "@mui/icons-material/Menu";
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import MailOutlinedIcon from '@mui/icons-material/MailOutlined';

type ExpandableMenuProps = {
    activeSection?: string;
    className?: string;
};

export default function ExpandableMenu({ activeSection, className }: ExpandableMenuProps) {
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={className}>
            <IconButton
                onClick={handleClick}
                aria-label="main menu"
                color="inherit"
                size="large"
                edge="end"
            >
                <MenuIcon />
            </IconButton>
            <Menu
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
            >
                <Link href="/work" passHref>
                    <MenuItem component="a" selected={activeSection === 'work'} onClick={handleClose}>
                        <ListItemIcon><WorkOutlineOutlinedIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Work" />
                    </MenuItem>
                </Link>
                <Link href="/about" passHref>
                    <MenuItem component="a" selected={activeSection === 'about'} onClick={handleClose}>
                        <ListItemIcon><InfoOutlinedIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="About" />
                    </MenuItem>
                </Link>
                <Link href="/contact" passHref>
                    <MenuItem component="a" selected={activeSection === 'contact'} onClick={handleClose}>
                        <ListItemIcon><MailOutlinedIcon fontSize="small" /></ListItemIcon>
                        <ListItemText primary="Contact" />
                    </MenuItem>
                </Link>
            </Menu>
        </div>
    );
}