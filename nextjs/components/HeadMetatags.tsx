import React from "react";
import Head from 'next/head';
import * as apiTypes from '../types/api';


type HeadMetatagsProps = {
    title?: string;
    siteName: string;
    description: string;
    image?: apiTypes.Image;
    type?: "article" | "website";
};

export default function HeadMetatags({ title, siteName, description, type, image }: HeadMetatagsProps) {
    return (
        <Head>
            <title>{title ? `${title} | ${siteName}` : siteName}</title>
            <meta name="description" content={description} />
            <meta property="og:site_name" content={siteName}/>
            <meta property="og:title" content={title ? title : siteName}/>
            <meta property="og:description" content={description} />
            <meta property="og:locale" content="en_CA" />
            <meta property="og:type" content={type ? type : "website"} />
            {image &&
                <>
                    <meta property="og:image" content={image.url} />
                    <meta property="og:image:width" content={`${image.width}`} />
                    <meta property="og:image:height" content={`${image.height}`} />
                </>
            }
        </Head>
    );
};