import axios from "axios";
import * as apiTypes from "../types/api";


const axiosInstance = axios.create({
    baseURL: `${process.env.NEXT_PUBLIC_API_BASE_URL}/`,
});

export async function getAllProjects() {
    return axiosInstance.get<{projects: apiTypes.ListProject[]; filters: apiTypes.ProjectFilters}>('projects/list/')
}

export async function getFeaturedProjects() {
    return axiosInstance.get<apiTypes.ListProject[]>('projects/list/featured/')
}

export async function getProject(slug: string) {
    return axiosInstance.get<apiTypes.DisplayProject>(`projects/view/${slug}/`)
}

export async function getProfile() {
    return axiosInstance.get<apiTypes.Profile>('profile/view/')
}

export async function getAboutSections() {
    return axiosInstance.get<apiTypes.AboutSection[]>('profile/about/list/')
}

export async function getFeaturedTestimonials() {
    return axiosInstance.get<apiTypes.Testimonial[]>('testimonials/list/featured/')
}