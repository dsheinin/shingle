from shingle.apps.portfolio.models import Project
from django.conf import settings
from django.db import transaction
import json
import logging
import requests

logger = logging.getLogger(__name__)


class ClientPaths:
    HOME = '/'
    WORK = '/work'
    ABOUT = '/about'
    CONTACT = '/contact'

    @classmethod
    def project_path(cls, slug):
        return f'{cls.WORK}/{slug}'

    @classmethod
    def get_all_paths(cls):
        paths=[cls.HOME, cls.WORK, cls.ABOUT, cls.CONTACT]
        for project in Project.objects.all():
            paths.append(cls.project_path(project.slug))
        return paths


def revalidate(paths):
    try:
        response = requests.post(
            f'{settings.NEXT_BASE_URL}/api/revalidate',
            data={
                'token': settings.NEXT_API_TOKEN,
                'paths': json.dumps(paths),
            }
        )
        if response.status_code == 401:
            # 401 means we failed to authenticate, which is a config error, so throw an exception.
            response.raise_for_status()
        elif response.status_code == 500:
            # 500 means that at least some paths didn't revalidate. This could be because we sent an old path, or
            # it could be just that revalidation is unstable. Either way, it's not critical. Timed revalidation will
            # still happen.
            # TODO: Send user a warning?
            logger.warning(f"On-demand revalidation failed for paths: {paths}.")

    except requests.exceptions.RequestException:
        # A request failure might mean we just don't have a functioning client, so don't raise the exception, but
        # it could also mean that there's a config error. Either way, logging the error is appropriate.
        logger.error("On-demand revalidation request failed.")


def revalidate_deferred(paths):
    transaction.on_commit(lambda: revalidate(paths))
