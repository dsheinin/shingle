from .models import Testimonial
from rest_framework import serializers


class TestimonialSerializer(serializers.ModelSerializer):
    client_display = serializers.StringRelatedField(source='client')

    class Meta:
        model = Testimonial
        fields = ['id', 'text', 'name', 'role', 'client_display']
