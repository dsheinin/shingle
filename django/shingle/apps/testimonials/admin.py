from .models import Testimonial
from shingle.apps.portfolio.models import Project
from shingle.util.client import revalidate_deferred, ClientPaths
from django.contrib import admin


@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'role', 'client', 'featured']
    list_editable = ['featured']

    def client_revalidate(self, obj):
        revalidate_paths = [ClientPaths.HOME]
        for project in Project.objects.filter(clients__testimonial__pk=obj.pk):
            revalidate_paths.append(ClientPaths.project_path(project.slug))
        revalidate_deferred(revalidate_paths)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        # TODO: track old client to revalidate project pages for previous client.
        self.client_revalidate(obj)

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        self.client_revalidate(obj)