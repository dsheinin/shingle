from django.urls import path
import shingle.apps.testimonials.api_views as testimonial_api_views

app_name = 'testimonial-api'
urlpatterns = [
    path('list/featured/', testimonial_api_views.TestimonialFeaturedListAPIView.as_view(),
         name='testimonial-featured-list'),
]
