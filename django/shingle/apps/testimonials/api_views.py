from .models import Testimonial
from .serializers import TestimonialSerializer
from rest_framework.generics import ListAPIView


class TestimonialFeaturedListAPIView(ListAPIView):
    serializer_class = TestimonialSerializer
    queryset = Testimonial.objects.filter(featured=True)
