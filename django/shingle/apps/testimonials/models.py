from django.db import models


class Testimonial(models.Model):
    text = models.TextField()
    name = models.CharField(max_length=100)
    role = models.CharField(max_length=100, blank=True)
    client = models.ForeignKey('portfolio.Client', on_delete=models.SET_NULL, null=True, blank=True)
    featured = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.name
