from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class MessageCreateTestCase(APITestCase):
    def test_message_create(self):
        url = reverse('contact-api:message-create')
        response = self.client.post(url, {'name': "Test name", 'email': "test@example.com", 'message': "Test message"})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)
