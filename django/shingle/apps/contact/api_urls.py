from django.urls import path
import shingle.apps.contact.api_views as contact_api_views

app_name = 'contact-api'
urlpatterns = [
    path('message/create/', contact_api_views.MessageCreateAPIView.as_view(), name='message-create'),
]
