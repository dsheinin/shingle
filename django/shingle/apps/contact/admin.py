from .models import Message
from django.contrib import admin


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'email', 'submitted_time']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
