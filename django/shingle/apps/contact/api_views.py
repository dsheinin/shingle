from .models import Message
from .serializers import MessageSerializer
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from rest_framework.generics import CreateAPIView


class MessageCreateAPIView(CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        message = serializer.save()

        # Send email notification of new application
        email = EmailMessage(
            subject=f"[Contact Message] {message.name}",
            body=render_to_string(
                template_name='contact/message_notice_email_body.html',
                context={
                    'message': message,
                },
            ),
            from_email=settings.DEFAULT_FROM_EMAIL,
            reply_to=[message.email],
            to=settings.CONTACT_NOTICE_RECIPIENTS,
        )
        email.send()

        return message
