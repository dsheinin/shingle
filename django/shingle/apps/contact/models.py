from django.db import models


class Message(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    message = models.TextField()
    submitted_time = models.DateTimeField()

    class Meta:
        ordering = ['-submitted_time']

    def __str__(self):
        return self.name
