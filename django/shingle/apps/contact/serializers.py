from .models import Message
from django.utils import timezone
from rest_framework import serializers


class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = ['name', 'email', 'message']

    def create(self, validated_data):
        return Message.objects.create(**validated_data, submitted_time=timezone.now())
