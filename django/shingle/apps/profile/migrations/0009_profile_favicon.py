# Generated by Django 3.2.13 on 2022-06-02 19:29

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0008_profile_footer'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='favicon',
            field=models.ImageField(blank=True, help_text='32x32 image in ICO or PNG format', upload_to='profile/', validators=[django.core.validators.FileExtensionValidator(['ico', 'png'])]),
        ),
    ]
