from .models import Profile, AboutSection
from .serializers import ProfileSerializer, AboutSectionSerializer
from rest_framework.generics import RetrieveAPIView, ListAPIView


class ProfileRetrieveAPIView(RetrieveAPIView):
    serializer_class = ProfileSerializer

    def get_object(self):
        profile = Profile.objects.first()
        self.check_object_permissions(self.request, profile)
        return profile


class AboutSectionListAPIView(ListAPIView):
    serializer_class = AboutSectionSerializer
    queryset = AboutSection.objects.all()
