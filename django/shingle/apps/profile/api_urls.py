from django.urls import path
import shingle.apps.profile.api_views as profile_api_views

app_name = 'profile-api'
urlpatterns = [
    path('view/', profile_api_views.ProfileRetrieveAPIView.as_view(), name='profile-view'),
    path('about/list/', profile_api_views.AboutSectionListAPIView.as_view(), name='about-list'),
]
