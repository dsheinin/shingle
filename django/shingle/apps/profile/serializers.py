from .models import Profile, AboutSection
from django.utils.html import strip_tags
from rest_framework import serializers


class ProfileSerializer(serializers.ModelSerializer):
    home_intro_plain = serializers.SerializerMethodField()
    about_intro_plain = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ['name', 'name_header', 'title', 'phone_display', 'email', 'location', 'svg_logo', 'favicon',
                  'home_intro', 'home_intro_plain', 'about_intro', 'about_intro_plain', 'work_intro', 'contact_intro',
                  'footer']

    def get_home_intro_plain(self, obj):
        return strip_tags(obj.home_intro)

    def get_about_intro_plain(self, obj):
        return strip_tags(obj.about_intro)


class AboutSectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutSection
        fields = '__all__'
