from django.core.validators import FileExtensionValidator
from django.db import models
from solo.models import SingletonModel
import phonenumbers
from phonenumber_field.modelfields import PhoneNumberField


class Profile(SingletonModel):
    name = models.CharField(max_length=100)
    name_header = models.BooleanField("Show name in header?", default=True)
    title = models.CharField(max_length=100)
    phone = PhoneNumberField(blank=True)
    email = models.EmailField(blank=True)
    location = models.CharField(max_length=100, blank=True)
    svg_logo = models.TextField(blank=True,
                                help_text="Paste optimized SVG XML. Fill/stroke colour will be applied with CSS. Set "
                                          "viewbox but not width/height in svg tag.")
    favicon = models.ImageField(blank=True, upload_to='profile/',
                                help_text='32x32 image in ICO or PNG format',
                                validators=[FileExtensionValidator(['ico', 'png'])])
    home_intro = models.TextField(blank=True)
    about_intro = models.TextField(blank=True)
    work_intro = models.CharField(max_length=255, blank=True, help_text="For meta description only. "
                                                                        "Suggested max length 160 characters.")
    contact_intro = models.CharField(max_length=255, blank=True, help_text="For meta description only. "
                                                                           "Suggested max length 160 characters.")
    footer = models.TextField(blank=True)

    def __str__(self):
        return 'Profile'

    def phone_display(self):
        """
        Format the number for display using dots as separators, and including the leading '1.' for toll free.
        """
        format = "\\1.\\2.\\3"
        nf = phonenumbers.NumberFormat(pattern="(\\d{3})(\\d{3})(\\d{4})", format=format)
        return phonenumbers.format_by_pattern(self.phone, phonenumbers.PhoneNumberFormat.NATIONAL, [nf])


class AboutSection(models.Model):
    title = models.CharField(max_length=100)
    svg_icon = models.TextField(blank=True,
                                help_text="Paste optimized SVG XML of a SQUARE icon. Fill colour will be applied with "
                                          "CSS.")
    text = models.TextField()
    rank = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['rank']

    def __str__(self):
        return self.title
