from .models import Profile, AboutSection
from shingle.util.client import revalidate_deferred, ClientPaths
from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget
from solo.admin import SingletonModelAdmin
from adminsortable2.admin import SortableAdminMixin


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        widgets = {
            'home_intro': CKEditorWidget,
            'about_intro': CKEditorWidget,
            'footer': CKEditorWidget('footer'),
        }


@admin.register(Profile)
class ProfileAdmin(SingletonModelAdmin):
    form = ProfileForm

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        revalidate_deferred(ClientPaths.get_all_paths())


class AboutSectionForm(forms.ModelForm):
    class Meta:
        model = AboutSection
        fields = '__all__'
        widgets = {
            'text': CKEditorWidget('about'),
        }


@admin.register(AboutSection)
class AboutSectionAdmin(SortableAdminMixin, admin.ModelAdmin):
    form = AboutSectionForm

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        revalidate_deferred([ClientPaths.ABOUT])

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        revalidate_deferred([ClientPaths.ABOUT])
