from django import forms
from django.utils.translation import gettext as _
from shingle.apps.portfolio.models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = '__all__'

    def clean(self):
        cleaned_data = super().clean()
        start_year = cleaned_data.get('start_year')
        end_year = cleaned_data.get('end_year')

        if start_year and end_year and start_year > end_year:
            msg = _("The end year can't be earlier than start year.")
            self.add_error('start_year', msg)
            self.add_error('end_year', msg)

        if end_year and not start_year:
            msg = _("If you enter an end year you must also enter a start year. (They can be the same.)")
            self.add_error('start_year', msg)

