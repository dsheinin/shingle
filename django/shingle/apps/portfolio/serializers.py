from .models import Project, Link, SimpleProjectAttribute, Section, Client
from shingle.apps.testimonials.serializers import TestimonialSerializer
from rest_framework import serializers
from sorl.thumbnail import get_thumbnail


class SorlImageField(serializers.ImageField):

    """Inspired by https://github.com/dessibelle/sorl-thumbnail-serializer-field"""

    def __init__(self, geometry_string, options=None, *args, **kwargs):
        self.geometry_string = geometry_string
        self.options = options or {}
        super().__init__(*args, **kwargs)

    def to_representation(self, value):
        if not value:
            return None

        image = get_thumbnail(value, self.geometry_string, **self.options)

        try:
            request = self.context.get('request', None)
            return {
                'url': request.build_absolute_uri(image.url),
                'width': image.width,
                'height': image.height,
            }
        except:
            return super().to_representation(image)


class LinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Link
        fields = ['id', 'url', 'text']


class SimpleProjectAttributeSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimpleProjectAttribute
        fields = ['id', 'name', 'text']


class SectionSerializer(serializers.ModelSerializer):
    image = SorlImageField('920x500', {'upscale': False})

    class Meta:
        model = Section
        fields = ['id', 'image', 'image_alt_text', 'caption', 'text']


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'name', 'url']


class ProjectSerializer(serializers.ModelSerializer):
    lead_image = SorlImageField('1280x540', {'crop': 'center'})
    og_image = SorlImageField('600x315', {'crop': 'center'}, source='lead_image')
    domains_display = serializers.StringRelatedField(source='ordered_domains', many=True)
    clients_display = ClientSerializer(source='ordered_clients', many=True)
    roles_display = serializers.StringRelatedField(source='ordered_roles', many=True)
    skills_display = serializers.StringRelatedField(source='ordered_skills', many=True)
    links = LinkSerializer(many=True)
    simple_attributes = SimpleProjectAttributeSerializer(many=True)
    sections = SectionSerializer(many=True)
    testimonials = TestimonialSerializer(many=True, source='get_testimonials')

    class Meta:
        model = Project
        fields = '__all__'


class ProjectListSerializer(serializers.ModelSerializer):
    thumbnail = SorlImageField('600x400', {'crop': 'center'}, source='lead_image')

    class Meta:
        model = Project
        fields = ['id', 'thumbnail', 'slug', 'title', 'summary', 'clients', 'domains', 'skills',
                  'roles', 'launched']


def filter_serializer(mdl):
    class FilterSerializer(serializers.ModelSerializer):
        project_count = serializers.IntegerField()

        class Meta:
            model = mdl
            fields = ['id', 'name', 'project_count']
    return FilterSerializer
