from shingle.apps.portfolio.forms import ProjectForm
from django.test import TestCase

class ProjectFormTests(TestCase):
    def test_invalid_year_range(self):
        """
        If the start year is later than the end year, both fields contain errors
        """
        form_data = {'start_year': 2016, 'end_year': 2015}
        form = ProjectForm(data=form_data)
        self.assertTrue(form.errors['start_year'])
        self.assertTrue(form.errors['end_year'])

    def test_single_year_range(self):
        """
        If the start year and the end year are the same, neither field contains errors
        """
        form_data = {'start_year': 2016, 'end_year': 2016}
        form = ProjectForm(data=form_data)
        self.assertFalse('start_year' in form.errors)
        self.assertFalse('end_year' in form.errors)
