from django.urls import path
import shingle.apps.portfolio.api_views as portfolio_api_views

app_name = 'portfolio-api'
urlpatterns = [
    path('list/', portfolio_api_views.ProjectListAPIView.as_view(), name='project-list'),
    path('list/featured/', portfolio_api_views.ProjectFeaturedListAPIView.as_view(), name='project-featured-list'),
    path('view/<slug:slug>/', portfolio_api_views.ProjectRetrieveAPIView.as_view(), name='project-view'),
]
