import shingle.apps.portfolio.models as portfolio_models
from shingle.apps.portfolio.forms import ProjectForm
from shingle.util.client import revalidate_deferred, ClientPaths
from django.forms import ModelForm
from django.contrib import admin
from django.contrib.admin.widgets import AdminTextareaWidget
from adminsortable2.admin import SortableInlineAdminMixin
from ckeditor.widgets import CKEditorWidget


class DomainInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.ProjectDomain
    extra = 0


class ClientInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.ProjectClient
    extra = 0


class RoleInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.ProjectRole
    extra = 0


class SkillInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.ProjectSkill
    extra = 0


class SimpleProjectAttributeInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.SimpleProjectAttribute
    extra = 0


class SectionAdminForm(ModelForm):
    class Meta:
        model = portfolio_models.Section
        fields = '__all__'
        widgets = {
            'caption': AdminTextareaWidget(attrs={'rows': 3}),
            'text': CKEditorWidget('article'),
        }


class SectionInline(SortableInlineAdminMixin, admin.StackedInline):
    form = SectionAdminForm
    model = portfolio_models.Section
    extra = 0


class LinkInline(SortableInlineAdminMixin, admin.TabularInline):
    model = portfolio_models.Link
    extra = 0


class ProjectAdminForm(ProjectForm):
    class Meta:
        widgets = {
            'summary': AdminTextareaWidget(attrs={'rows': 3}),
        }


@admin.register(portfolio_models.Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'published', 'featured']
    list_editable = ['published', 'featured']
    form = ProjectAdminForm
    inlines = [DomainInline, ClientInline, RoleInline, SkillInline, SimpleProjectAttributeInline, LinkInline,
               SectionInline]
    prepopulated_fields = {"slug": ("title",)}

    def client_revalidate(self, obj, revalidate_project=True):
        revalidate_paths = [ClientPaths.HOME, ClientPaths.WORK]
        if revalidate_project:
            revalidate_paths.append(ClientPaths.project_path(obj.slug))
        revalidate_deferred(revalidate_paths)

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        # Can't revalidate the project page itself if it's new or if the slug has changed. In this case, Next.js
        # "blocking" fallback will create the new page on first request. (And, in case of delete, old page will be
        # removed by rebuild or automatic revalidation.)
        # TODO: track old slug to revalidate (remove) page for changed slug.
        self.client_revalidate(obj, revalidate_project=(change and 'slug' not in form.changed_data))

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        self.client_revalidate(obj)


admin.site.register(portfolio_models.Client)
admin.site.register(portfolio_models.Skill)
admin.site.register(portfolio_models.Domain)
admin.site.register(portfolio_models.Role)
