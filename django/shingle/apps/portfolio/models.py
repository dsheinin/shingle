from django.db import models
from django.urls import reverse
from django.db.models import F


class CategoricalAttribute(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['name']

    def __str__(self):
        return self.name


class Domain(CategoricalAttribute):
    pass


class Client(CategoricalAttribute):
    url = models.URLField(blank=True, max_length=255)


class Role(CategoricalAttribute):
    pass


class Skill(CategoricalAttribute):
    pass


class Project(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    lead_image = models.ImageField(null=True, blank=True, upload_to='portfolio/lead/')
    summary = models.TextField()
    description = models.TextField(blank=True)
    start_year = models.PositiveSmallIntegerField(null=True, blank=True)
    end_year = models.PositiveSmallIntegerField(null=True, blank=True)
    domains = models.ManyToManyField(Domain, through='ProjectDomain', blank=True)
    clients = models.ManyToManyField(Client, through='ProjectClient', blank=True)
    roles = models.ManyToManyField(Role, through='ProjectRole', blank=True)
    skills = models.ManyToManyField(Skill, through='ProjectSkill', blank=True)
    published = models.BooleanField(default=True, blank=True)
    featured = models.BooleanField(default=False, blank=True)
    launched = models.BooleanField(default=True, blank=True)

    class Meta:
        ordering = ['-featured', F('end_year').desc(nulls_first=True), '-start_year', 'title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('portfolio:project', args=[self.slug])

    def get_testimonials(self):
        from shingle.apps.testimonials.models import Testimonial
        return Testimonial.objects.filter(client__project=self)

    def ordered_domains(self):
        return self.domains.order_by('projectdomain')

    def ordered_clients(self):
        return self.clients.order_by('projectclient')

    def ordered_roles(self):
        return self.roles.order_by('projectrole')

    def ordered_skills(self):
        return self.skills.order_by('projectskill')


class ProjectAttribute(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    rank = models.PositiveSmallIntegerField(default=0)

    class Meta:
        abstract = True
        ordering = ['rank']


class ProjectDomain(ProjectAttribute):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)

    class Meta(ProjectAttribute.Meta):
        unique_together = ['domain', 'project']

    def __str__(self):
        return f'{self.project.title}: {self.domain.name}'


class ProjectClient(ProjectAttribute):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Meta(ProjectAttribute.Meta):
        unique_together = ['client', 'project']

    def __str__(self):
        return f'{self.project.title}: {self.client.name}'


class ProjectRole(ProjectAttribute):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    class Meta(ProjectAttribute.Meta):
        unique_together = ['role', 'project']

    def __str__(self):
        return f'{self.project.title}: {self.role.name}'


class ProjectSkill(ProjectAttribute):
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)

    class Meta(ProjectAttribute.Meta):
        unique_together = ['skill', 'project']

    def __str__(self):
        return f'{self.project.title}: {self.skill.name}'


class SimpleProjectAttribute(models.Model):
    name = models.CharField(max_length=30)
    text = models.CharField(max_length=255)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='simple_attributes')
    rank = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['rank']

    def __str__(self):
        return self.name


class Section(models.Model):
    image = models.ImageField(blank=True, upload_to='portfolio/section/')
    image_alt_text = models.CharField(max_length=100, blank=True)
    caption = models.TextField(blank=True)
    text = models.TextField(blank=True)
    rank = models.PositiveSmallIntegerField(default=0)
    project = models.ForeignKey(Project, models.CASCADE, related_name='sections')

    class Meta:
        ordering = ['rank']

    def __str__(self):
        return f'{self.project.title}: Section {self.rank}'


class Link(models.Model):
    url = models.URLField(max_length=255)
    text = models.CharField(max_length=100)
    rank = models.PositiveSmallIntegerField(default=0)
    project = models.ForeignKey(Project, models.CASCADE, related_name='links')

    class Meta:
        ordering = ['rank']

    def __str__(self):
        return self.text