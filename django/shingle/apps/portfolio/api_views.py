from .models import Project, Role, Client, Skill, Domain
from .serializers import ProjectSerializer, ProjectListSerializer, filter_serializer
from django.db.models import Count
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response


class ProjectListAPIView(ListAPIView):
    serializer_class = ProjectListSerializer
    queryset = Project.objects.filter(published=True)

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        projects = response.data
        qs = self.get_queryset()
        filters = {
            key: {
                'title': title,
                'items': filter_serializer(klass)(
                    klass.objects
                        .filter(project__in=qs).distinct()
                        .annotate(project_count=Count('project'))
                        .order_by('-project_count'),
                    many=True
                ).data,
            } for key, klass, title in [
                #('clients', Client, 'Client'),
                ('roles', Role, 'My Role'),
                ('skills', Skill, 'Skill'),
                #('domains', Domain, 'Domain'),
            ]
        }
        return Response({'projects': projects, 'filters': filters})


class ProjectFeaturedListAPIView(ListAPIView):
    serializer_class = ProjectListSerializer
    queryset = Project.objects.filter(published=True, featured=True)


class ProjectRetrieveAPIView(RetrieveAPIView):
    serializer_class = ProjectSerializer
    lookup_field = 'slug'
    queryset = Project.objects.filter(published=True)
