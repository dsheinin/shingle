from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/projects/', include('shingle.apps.portfolio.api_urls')),
    path('api/profile/', include('shingle.apps.profile.api_urls')),
    path('api/testimonials/', include('shingle.apps.testimonials.api_urls')),
    path('api/contact/', include('shingle.apps.contact.api_urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
